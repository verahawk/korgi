# Contributing guidelines

## Installation from the source

1. Clone this repository

    `git clone https://invent.kde.org/verahawk/korgi && cd korgi`

1. Install required build dependencies

    `./tools/install-build-dependencies.sh`

1. Build

    `cmake -B build/ . && cmake --build build/`

1. Install

    `sudo cmake --install build/`

To uninstall run `sudo make uninstall`.

## Code formatting

This project uses [EditorConfig](https://editorconfig.org/) to maintain consistent coding style. It's advised to use an IDE that can format the source code according to the `.editorconfig` file.

This project adheres to the [KDE coding style guidelines](https://community.kde.org/Policies/Frameworks_Coding_Style). Before submitting a MR, please format your code using the clang-format target: `make clang-format`.
