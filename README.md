# Korgi

A simple program that allows the user to easily organize their tasks.

Features:

- Simple, standalone application
- Native look & feel
- Easy and fast to navigate
- Fully convergent

## Installation

Currently, Korgi binaries aren't available in any public repository. To install it, you will have to complile it by yourself.

See [CONTRIBUTING.md](CONTRIBUTING.md) for more details.

## Gallery

<div align="center">
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_normal_1.png" alt="Main window of the Korgi program" height="460"/>
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_narrow_1.png" alt="Main window of the Korgi program (narrow view)" height="460"/>
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_narrow_4.png" alt="Task details in a narrow view" height="460"/>
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_normal_2.png" alt="Adding a new task to the list" height="460"/>
</div>

<div align="center">
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_normal_3.png" alt="Main window of the Korgi program with some tasks added" height="460"/>
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_narrow_3.png" alt="Main window of the Korgi program with some tasks added (narrow view)" height="460"/>
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_narrow_2.png" alt="Side drawer expanded in a narrow view" height="460"/>
    <img src="https://invent.kde.org/verahawk/korgi/-/raw/media/screenshot_normal_4.png" alt="Main window with one task marked as completed" height="460"/>
</div>

## License

    Korgi – A simple task list manager
    Copyright © 2021 Ignacy Kajdan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
