# Save file format

This document contains a description of the save file produced by Korgi.

## Example save file

Note: The following listing has been prettified for readability reasons. Korgi saves it's data in a minified form.

```
{
  "tasks": [
    {
      "desc": "Foo",
      "done": false,
      "title": "Untitled task"
    },
    {
      "desc": "Bar",
      "done": true,
      "title": "Hello, World!"
    }
  ],
  "version": "0"
}
```

## Top level objects

### version

The major version of Korgi with which this file was generated.

### tasks

An array of tasks.

## Task data

### desc

Task description.

### done

Task done status.

### title

Task title.
