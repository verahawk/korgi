/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami
import QtQuick.Controls 2.0 as Controls
import QtQuick.Layouts 1.2

Kirigami.ScrollablePage {
    id: addEditPage

    property bool newTask: true
    property int taskIndex: -1
    property string taskTitle: ""
    property string taskDescription: ""

    title: newTask ? i18n("New task") : i18n("Edit task")
    Keys.onEscapePressed: pageStack.goBack() // Close the page using ESC key

    // Toolbar action buttons
    actions {
        main: Kirigami.Action {
            iconName: "entry-delete"
            text: i18n("Delete")
            tooltip: i18n("Delete this task")
            visible: !newTask
            onTriggered: {
                korgiTaskList.removeTask(korgiTaskListModel.mapToSource(korgiTaskListModel.index(taskIndex, 0)))
                pageStack.goBack()
            }
        }
    }

    ColumnLayout {
        // Title field
        Controls.TextField {
            id: titleField
            Layout.fillWidth: true
            placeholderText: i18n("Title")
            text: newTask ? "" : taskTitle
            onAccepted: descriptionField.forceActiveFocus()
            focus: true
        }

        // Description field
        Controls.TextArea {
            id: descriptionField
            Layout.fillWidth: true
            Layout.minimumHeight: Kirigami.Units.gridUnit * 5
            placeholderText: i18n("Description")
            text: newTask ? "" : taskDescription
            wrapMode: Controls.TextArea.Wrap
            KeyNavigation.tab: doneButton
            KeyNavigation.backtab: titleField
            KeyNavigation.priority: KeyNavigation.BeforeItem
        }
    }

    footer: Controls.ToolBar {
        RowLayout {
            anchors.fill: parent
            // Done button
            Controls.Button {
                Layout.fillWidth: true
                id: doneButton
                text: newTask ? i18n("Add") : i18n("Save")
                icon.name: newTask ? "list-add" : "document-save"
                KeyNavigation.tab: titleField
                KeyNavigation.backtab: descriptionField
                KeyNavigation.priority: KeyNavigation.BeforeItem
                Keys.onReturnPressed: doneButton.clicked() // Accept the form with enter or numpad enter keys
                Keys.onEnterPressed: doneButton.clicked()
                enabled: taskTitle !== titleField.text || taskDescription !== descriptionField.text // Only enable when something has changed
                onClicked: {
                    if (newTask) {
                        korgiTaskList.addTask([titleField.text, descriptionField.text]) // Append new task
                    } else {
                        korgiTaskList.editTask(korgiTaskListModel.mapToSource(korgiTaskListModel.index(taskIndex, 0)),
                                               [titleField.text, descriptionField.text]) // Edit a task
                    }
                    pageStack.goBack();
                }
            }
        }
    }

    Component.onCompleted: titleField.focus = true // Focus title field by default
}

