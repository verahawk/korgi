/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2

Kirigami.ScrollablePage {
    id: mainPage
    title: i18n("Tasks")

    // About page
    Component {
        id: aboutPage
        Kirigami.AboutPage {
            aboutData: korgiAboutData
        }
    }

    // Sort menu group
    Controls.ActionGroup {
        id: sortGroup
        enabled: false // FIXME: Disabled for now
    }

    // Toolbar action buttons
    actions {
        main: Kirigami.Action {
            icon.name: "list-add"
            text: root.wideScreen ? i18n("New task") : ""
            tooltip: i18n("Add a new task")
            shortcut: StandardKey.New
            onTriggered: {
                pageStack.push(Qt.resolvedUrl("AddEditPage.qml"), { newTask: true });
            }
        }
        contextualActions: [
            Kirigami.Action {
                icon.name: "view-sort-descending-symbolic"
                text: i18n("&Sort by")
                tooltip: i18n("Change the sorting order")
                displayHint: Kirigami.Action.DisplayHint.AlwaysHide
                Kirigami.Action {
                    id: sortName
                    icon.name: "sort-name"
                    text: i18n("&Name")
                    checked: true
                    checkable: true
                    Controls.ActionGroup.group: sortGroup
                }
                Kirigami.Action {
                    id: sortPriority
                    icon.name: "emblem-important-symbolic"
                    text: i18n("&Priority")
                    checkable: true
                    Controls.ActionGroup.group: sortGroup
                }
                Kirigami.Action {
                    id: sortCustom
                    icon.name: "adjustrow"
                    text: i18n("&Custom")
                    checkable: true
                    Controls.ActionGroup.group: sortGroup
                }
            },
            Kirigami.Action {
                icon.name: "edit-clear-all"
                text: i18n("&Clear done")
                tooltip: i18n("Clear tasks marked as done")
                onTriggered: korgiTaskList.removeDoneTasks()
                displayHint: Kirigami.Action.DisplayHint.AlwaysHide
            },
            Kirigami.Action {
                icon.name: "view-hidden"
                text: i18n("Show &done")
                tooltip: i18n("View tasks marked as done")
                shortcut: "Ctrl+H"
                checkable: true
                checked: true
                onCheckedChanged: korgiTaskListModel.doneFilterEnabled = !checked
                displayHint: Kirigami.Action.DisplayHint.AlwaysHide
            },
            Kirigami.Action {
                icon.name: "help-hint"
                text: i18n("&About")
                tooltip: i18n("Show information about Korgi")
                displayHint: Kirigami.Action.DisplayHint.AlwaysHide
                onTriggered: pageStack.layers.push(aboutPage)
            }
        ]
    }

    // Show greeter when the task list is empty
    Kirigami.PlaceholderMessage {
        anchors.centerIn: parent
        visible: taskList.count === 0
        icon.name: "checkmark"
        text: i18n("Task list is empty")
    }

    // Task list
    Component {
        id: taskListDelegate
        Kirigami.SwipeListItem {
            id: listItem
            implicitWidth: taskListDelegate.implicitWidth ? taskListDelegate.implicitWidth : ""
            onClicked: {
                pageStack.push(Qt.resolvedUrl("AddEditPage.qml"), { newTask: false, taskIndex: model.index, taskTitle: model.title,
                                                                    taskDescription: model.description });
            }
            contentItem: RowLayout {
                Kirigami.ListItemDragHandle {
                    listItem: listItem
                    listView: taskList
                    visible: sortGroup.checkedAction == sortCustom;
                    onMoveRequested: korgiTaskList.moveTask(korgiTaskListModel.mapToSource(korgiTaskListModel.index(oldIndex, 0)),
                                                            korgiTaskListModel.mapToSource(korgiTaskListModel.index(newIndex, 0)));
                }
                Controls.CheckBox {
                    checked: done
                    onClicked: model.done = !model.done
                }
                Controls.Label {
                    Layout.fillWidth: true
                    Layout.maximumWidth: root.wideScreen ? parent.width / 3 : -1
                    maximumLineCount: 1
                    elide: Text.ElideRight
                    text: title
                    font.strikeout: done
                    opacity: done ? 0.65 : 1
                }
                Controls.Label {
                    Layout.fillWidth: true
                    Layout.maximumWidth: parent.width * 2 / 3 - Kirigami.Units.gridUnit * 2
                    leftPadding: Kirigami.Units.gridUnit
                    maximumLineCount: 1
                    elide: Text.ElideRight
                    text: description
                    font.strikeout: done
                    font.italic: true
                    opacity: done ? 0.3 : 0.65
                    visible: root.wideScreen
                }
            }

            // Task actions
            actions: [
                Kirigami.Action {
                    text: i18n("Delete")
                    iconName: "entry-delete"
                    visible: true
                    // index: integer of the current row
                    // model.index(index, 0): QModelIndex of the current row
                    // model.mapToSource(model.index(index, 0): QModelIndex of the proxied model row
                    onTriggered: korgiTaskList.removeTask(korgiTaskListModel.mapToSource(korgiTaskListModel.index(index, 0)));
                }
            ]
        }
    }

    ListView {
        id: taskList
        model: korgiTaskListModel
        currentIndex: -1 // Don't highlight any entry

        // Reordering animation
        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }

        delegate: Kirigami.DelegateRecycler {
            width: parent ? parent.width : 0
            sourceComponent: taskListDelegate
        }
    }
}
