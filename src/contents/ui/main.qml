/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

import QtQuick 2.6
import org.kde.kirigami 2.14 as Kirigami

Kirigami.ApplicationWindow {
    id: root
    title: i18n("Korgi")
    minimumWidth: Kirigami.Units.gridUnit * 16
    minimumHeight: Kirigami.Units.gridUnit * 16
    wideScreen: width >= Kirigami.Units.gridUnit * 25

    pageStack.initialPage: MainPage { id: mainPage }
    pageStack.defaultColumnWidth: width // Show only one page at a time
    pageStack.globalToolBar.showNavigationButtons: pageStack.currentIndex == 0 // Show the back button when needed
                                                   ? Kirigami.ApplicationHeaderStyle.NoNavigationButtons
                                                   : Kirigami.ApplicationHeaderStyle.ShowBackButton

    onClosing: korgiTaskList.save() // Save tasks on exit
}
