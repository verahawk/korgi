/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

#include "donefilterproxymodel.h"
#include "tasklistmodel.h"

DoneFilterProxyModel::DoneFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
    , doneFilterEnabled_(false)
{
}

bool DoneFilterProxyModel::doneFilterEnabled() const
{
    return doneFilterEnabled_;
}

void DoneFilterProxyModel::setDoneFilterEnabled(bool state)
{
    if (doneFilterEnabled_ == state) {
        return;
    }

    doneFilterEnabled_ = state;
    emit doneFilterEnabledChanged();

    invalidateFilter();
}

bool DoneFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (!doneFilterEnabled_) {
        return true;
    }

    const QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    return !index.data(TaskListModel::DoneRole).toBool();
}
