/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

#ifndef DONEFILTERPROXYMODEL_H
#define DONEFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>

class DoneFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(bool doneFilterEnabled READ doneFilterEnabled WRITE setDoneFilterEnabled NOTIFY doneFilterEnabledChanged)

public:
    explicit DoneFilterProxyModel(QObject *parent = nullptr);
    bool doneFilterEnabled() const;

signals:
    void doneFilterEnabledChanged();

public slots:
    void setDoneFilterEnabled(bool state);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
    bool doneFilterEnabled_;
};

#endif // DONEFILTERPROXYMODEL_H
