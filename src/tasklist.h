/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

#ifndef TASKLIST_H
#define TASKLIST_H

#include <QFileSystemWatcher>
#include <QObject>
#include <QVector>

struct Task {
    QString title;
    QString description;
    bool done;
};

class TaskList : public QObject
{
    Q_OBJECT

public:
    explicit TaskList(QObject *parent = nullptr);
    QVector<Task> tasks() const;
    bool setItemAt(int index, const Task &task);

signals:
    void preTaskAdded();
    void postTaskAdded();

    void postTaskEdited(const int index);

    void preTaskMoved(const int from, const int to);
    void postTaskMoved();

    void preTaskRemoved(const int index);
    void postTaskRemoved();

    void preModelReset();
    void postModelReset();

public slots:
    void addTask(const QStringList task);
    void editTask(const QModelIndex index, const QStringList task);
    void moveTask(const QModelIndex from, const QModelIndex to);
    void removeTask(const QModelIndex index);
    void removeDoneTasks();
    void save() const; // Save tasks to a file
    void load(); // Load tasks from a file

private:
    QVector<Task> tasks_;
    const QString save_directory_;
    const QString save_file_;
    QFileSystemWatcher watcher_;
};

#endif // TASKLIST_H
