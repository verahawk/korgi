/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

#include "tasklistmodel.h"
#include "tasklist.h"

TaskListModel::TaskListModel(QObject *parent)
    : QAbstractListModel(parent)
    , model_(nullptr)
{
}

int TaskListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid() || !model_) {
        return 0;
    }

    return model_->tasks().size();
}

QVariant TaskListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !model_) {
        return QVariant();
    }

    const Task &item = model_->tasks().at(index.row());
    switch (role) {
    case TitleRole:
        return QVariant(item.title);
    case DescriptionRole:
        return QVariant(item.description);
    case DoneRole:
        return QVariant(item.done);
    }

    return QVariant();
}

bool TaskListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!model_) {
        return false;
    }

    Task item = model_->tasks().at(index.row());
    switch (role) {
    case TitleRole:
        item.title = value.toString();
        break;
    case DescriptionRole:
        item.description = value.toString();
        break;
    case DoneRole:
        item.done = value.toBool();
        break;
    }

    if (model_->setItemAt(index.row(), item)) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }

    return false;
}

Qt::ItemFlags TaskListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> TaskListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[TitleRole] = "title";
    names[DescriptionRole] = "description";
    names[DoneRole] = "done";
    return names;
}

TaskList *TaskListModel::list() const
{
    return model_;
}

void TaskListModel::setList(TaskList *list)
{
    beginResetModel();

    if (model_) {
        model_->disconnect(this);
    }

    model_ = list;

    if (model_) {
        // Added
        connect(model_, &TaskList::preTaskAdded, this, [=]() {
            const int index = model_->tasks().size();
            beginInsertRows(QModelIndex(), index, index);
        });
        connect(model_, &TaskList::postTaskAdded, this, [=]() {
            endInsertRows();
        });

        // Edited
        connect(model_, &TaskList::postTaskEdited, this, [=](const int task_index) {
            emit dataChanged(index(task_index), index(task_index));
        });

        // Moved
        connect(model_, &TaskList::preTaskMoved, this, [=](const int from, const int to) {
            beginMoveRows(QModelIndex(), from, from, QModelIndex(), from < to ? to + 1 : to);
        });
        connect(model_, &TaskList::postTaskMoved, this, [=]() {
            endMoveRows();
        });

        // Removed
        connect(model_, &TaskList::preTaskRemoved, this, [=](const int task_index) {
            beginRemoveRows(QModelIndex(), task_index, task_index);
        });
        connect(model_, &TaskList::postTaskRemoved, this, [=]() {
            endRemoveRows();
        });

        // Reset
        connect(model_, &TaskList::preModelReset, this, [=]() {
            beginResetModel();
        });
        connect(model_, &TaskList::postModelReset, this, [=]() {
            endResetModel();
        });
    }

    endResetModel();
}
