/*
 *  SPDX-License-Identifier: GPL-3.0-or-later
 *  SPDX-FileCopyrightText: 2021 Ignacy Kajdan <ignacy.kajdan@gmail.com>
 */

#ifndef TASKLISTMODEL_H
#define TASKLISTMODEL_H

#include <QAbstractListModel>

class TaskList;

class TaskListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(TaskList *list READ list WRITE setList)

public:
    enum Roles {
        TitleRole = Qt::UserRole,
        DescriptionRole,
        DoneRole,
    };

    explicit TaskListModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    TaskList *list() const;
    void setList(TaskList *list);

private:
    TaskList *model_;
};

#endif // TASKLISTMODEL_H
