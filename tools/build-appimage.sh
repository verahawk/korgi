#!/bin/sh
# Builds AppImage. The repository is copied into a temporary volume to avoid
# messing up permissions.

# Exit immediately on error
set -e

# Determine the absolute path to the repository
repo_dir_path="$(unset CDPATH && cd "$(dirname "$0")/.." && echo "$PWD")"

if [ "$(basename "${repo_dir_path}")" = korgi ]; then
    printf '%s %s\n' 'Repository path:' "${repo_dir_path}" >&2
else
    printf '%s\n' 'Cannot clearly determine the repository root. Bailing out.' >&2
    exit 1
fi

# Create temporary directory
temp_volume="$(mktemp -d)"
cp -r "${repo_dir_path}"/* "${temp_volume}"

# Create output directory
output_dir_path=/tmp/korgi-appimage
mkdir -p "${output_dir_path}"

# Build AppImage
cat <<- 'EOF' | docker run -i --rm --volume "${temp_volume}":/korgi:Z --volume "${output_dir_path}":/out:Z ubuntu
    set -e
    export DEBIAN_FRONTEND=noninteractive

    apt --assume-yes update
    apt --assume-yes install python3-pip python3-setuptools patchelf desktop-file-utils libgdk-pixbuf2.0-dev fakeroot strace fuse appstream wget
    wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage -O /opt/appimagetool
    cd /opt/
    chmod +x appimagetool
    sed -i 's|AI\x02|\x00\x00\x00|' appimagetool
    ./appimagetool --appimage-extract
    mv /opt/squashfs-root /opt/appimagetool.AppDir
    ln -s /opt/appimagetool.AppDir/AppRun /usr/local/bin/appimagetool
    pip3 install appimage-builder

    cd /korgi
    [ -d build ] && rm -r build
    appimage-builder --recipe ./data/AppImageBuilder.yml --skip-test
    mv Korgi*.AppImage /out
    rm -rf appimage-builder-cache

    exit 0
EOF

# Delete temporary volume
rm -r "${temp_volume}"

printf '\n%s %s\n' 'AppImage saved to' "${output_dir_path}/$(/usr/bin/ls ${output_dir_path})"
printf '\n\033[1m%s\033[0m\n' 'Success!'

exit 0
