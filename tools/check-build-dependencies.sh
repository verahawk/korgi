#!/bin/sh
# Checks if `install-build-dependencies.sh` script installs all required build
# dependencies. The repository is copied into a temporary volume to avoid
# messing up permissions.

# Exit immediately on error
set -e

# Determine the absolute path to the repository
repo_dir_path="$(unset CDPATH && cd "$(dirname "$0")/.." && echo "$PWD")"

if [ "$(basename "${repo_dir_path}")" = korgi ]; then
    printf '%s %s\n' 'Repository path:' "${repo_dir_path}" >&2
else
    printf '%s\n' 'Cannot clearly determine the repository root. Bailing out.' >&2
    exit 1
fi

# Loop over supported systems
for system in fedora ubuntu; do
    printf '\n\033[1m%s %s...\033[0m\n\n' 'Testing on' "${system}"

    temp_volume="$(mktemp -d)"
    cp -r "${repo_dir_path}"/* "${temp_volume}"

    cat <<- 'EOF' | docker run -i --rm --volume "${temp_volume}":/korgi:Z "${system}"
        set -e
        cd /korgi
        [ -d build ] && rm -r build
        command -v apt > /dev/null && export DEBIAN_FRONTEND=noninteractive && apt update
        ./tools/install-build-dependencies.sh --yes
        cmake -B build/ .
        cmake --build build/
        exit 0
	EOF
    # NOTE: The above line must be indented with a tab

    # Delete temporary volume
    rm -r "${temp_volume}"

    printf '\n%s\n' 'Done.'
done

printf '\n\033[1m%s\033[0m\n' 'Success!'

exit 0
