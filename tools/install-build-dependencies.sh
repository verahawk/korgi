#!/bin/bash
# shellcheck disable=SC2086
# Installs required build dependencies. Currently only RPM and DEB-based systems
# are supported.

# Exit immediately on error
set -e

# Package list for RPM-based systems
rpm_packages=(
    gcc-c++
    cmake
    extra-cmake-modules
    'cmake(Qt5)'
    'cmake(Qt5Qml)'
    'cmake(Qt5QuickControls2)'
    'cmake(Qt5Svg)'
    'cmake(KF5Kirigami2)'
    'cmake(KF5I18n)'
    'cmake(KF5CoreAddons)'
)

# Package list for DEB-based systems
deb_packages=(
    g++
    cmake
    extra-cmake-modules
    gettext
    qtdeclarative5-dev
    qtquickcontrols2-5-dev
    libqt5svg5-dev
    kirigami2-dev
    libkf5i18n-dev
    libkf5coreaddons-dev
)

# Privilege escalation method
if [ "$(id -u)" -eq 0 ]; then
    root_cmd=
else
    root_cmd='sudo'
fi

# Auto-confirmation for non-interactive environments
if [ "$1" = '--yes' ]; then
    dnf_options='--assumeyes'
    apt_options='--assume-yes'
else
    dnf_options=
    apt_options=
fi

# Determine installed package manager
if command -v rpm > /dev/null; then
    printf '%s %s\n\n' 'Installing using DNF:' "${rpm_packages[*]}"
    ${root_cmd} dnf ${dnf_options} install ${rpm_packages[*]}
elif command -v apt > /dev/null; then
    printf '%s %s\n\n' 'Installing using APT:' "${deb_packages[*]}"
    ${root_cmd} apt ${apt_options} install ${deb_packages[*]}
else
    printf '%s\n%s\n' \
           'This system seems to be neither RPM or DEB-based.' \
           'You will have to install all the dependencies manually.'
fi
